using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;
using WebApi.Repositories.Customer;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : Controller
    {
        CustomerDbContext db;

        public CustomerController(CustomerDbContext context)
        {
            db = context;
            if (!db.Customers.Any())
            {
                db.Customers.Add(new Customer { Firstname = "�������", Lastname = "���������" });
                db.Customers.Add(new Customer { Firstname = "�������", Lastname = "������" });
                db.SaveChanges();
            }
        }

        [HttpGet("{id:long}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            Customer user = await db.Customers.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                return NotFound();
            return new ObjectResult(user);
        }

        [HttpPost("")]   
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            if (customer == null)
            {
                return BadRequest();
            }
            db.Customers.Add(customer);
            await db.SaveChangesAsync();
            return Ok(customer.Id);
        }
    }
}