﻿using Microsoft.EntityFrameworkCore;

namespace WebApi.Repositories.Customer
{
    public class CustomerDbContext : DbContext
    {
        public DbSet<WebApi.Models.Customer> Customers { get; set; }

        /// <summary>
        /// Конструктор контекста
        /// </summary>
        public CustomerDbContext(DbContextOptions<CustomerDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
