﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public class WebApiService : IWebApiService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public WebApiService(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Customer> GetCustomer(long id)
        {
            var client = _httpClientFactory.CreateClient("web-api");

            using var response = await client.GetAsync($"{id}");
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Customer>(content);
        }

        public async Task<int> CreateCustomer(Customer customer)
        {
            var client = _httpClientFactory.CreateClient("web-api");
            var customerRequest = new CustomerCreateRequest(customer.Firstname, customer.Lastname);

            var content = new StringContent(JsonConvert.SerializeObject(customerRequest), Encoding.UTF8);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await client.PostAsync(client.BaseAddress, content);

            return int.Parse(await response.Content.ReadAsStringAsync());
        }
    }
}
