﻿using System;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public class ConsoleService : IConsoleService
    {

        private readonly bool _work = true;
        private readonly IWebApiService _wepApiService;
        private readonly IGeneratorService _generatorService;
        public ConsoleService(IWebApiService service, IGeneratorService generator)
        {
            _wepApiService = service;
            _generatorService = generator;
        }
        public async Task Start()
        {
            while (_work)
            {
                Console.WriteLine("Введите команду: \n" +
                    "get - для получения customer по идентификатору\n" +
                    "post - для генерации и отправки customer на сервер\n" +
                    "exit - для выхода из программы\n");

                var command = Console.ReadLine().ToLower();

                if (command == "exit")
                    break;

                if (command == "get")
                {
                    Console.WriteLine("Введите id: ");

                    var id = long.Parse(Console.ReadLine());

                    var customer = await _wepApiService.GetCustomer(id);

                    Console.WriteLine($"Id: {customer?.Id}; FirstName: {customer?.Firstname}; LastName: {customer?.Lastname}\n");
                }
                else if (command == "post")
                {
                    var customer = _generatorService.Generate();

                    Console.WriteLine("Сгенерированный customer: \n" +
                        $"FirstName: {customer?.Firstname}; LastName: {customer?.Lastname}\n");
                    Console.WriteLine("Для отправки введите post\n");

                    var subcommand = Console.ReadLine().ToLower();
                    if (subcommand == "post")
                    {
                        var id = await _wepApiService.CreateCustomer(customer);
                        Console.WriteLine($"Создан customer с id: {id}");
                    }
                    else
                    {
                        Console.WriteLine("Customer не отправлен\n");
                    }
                }
                else
                {
                    Console.WriteLine("Команда не распознана");
                }
            }
        }
    }
}
