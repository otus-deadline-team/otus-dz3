﻿namespace WebClient.Services
{
    public interface IGeneratorService
    {
        /// <summary>
        /// Генерирует пользователя
        /// </summary>
        /// <returns>Сгенерированного пользователя</returns>
        Customer Generate();
    }
}
