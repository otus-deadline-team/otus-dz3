﻿using System.Threading.Tasks;

namespace WebClient.Services
{
    public interface IConsoleService
    {
        public Task Start();
    }
}
