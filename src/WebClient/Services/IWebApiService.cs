﻿using System.Threading.Tasks;

namespace WebClient.Services
{
    public interface IWebApiService
    {
        /// <summary>
        /// Получить пользователя по идентификатору
        /// </summary>
        /// <param name="id">идентификатор пользователя</param>
        /// <returns>Пользователя</returns>
        Task<Customer> GetCustomer(long id);
        /// <summary>
        /// Создать пользователя
        /// </summary>
        /// <param name="customer">пользователь</param>
        /// <returns>Идентификатор нового пользователя</returns>
        Task<int> CreateCustomer(Customer customer);
    }
}
